class Acciona
  include XML::XmlAcciona
  attr_accessor :outward_day

  def initialize params
    @outward_day    = params[:outward_day]
    @outward_month  = params[:outward_month]
    @outward_year   = params[:outward_year]
    @outward_hour   = params[:outward_hour]
    @outward_minutes= params[:outward_minutes]
    @arrive_day     = params[:arrive_day]
    @arrive_month   = params[:arrive_month]
    @arrive_year    = params[:arrive_year]
    @arrive_hour    = params[:arrive_hour]
    @arrive_minutes = params[:arrive_minutes]
    @origin         = params[:origin]
    @destiny        = params[:destiny]
    @pets           = params[:pets]
    @passengers     = params[:passengers]
    @adults         = params[:adults]
    @children       = params[:children]
    @babies         = params[:babies]
    @vehicles       = params[:vehicles]
    @autos          = params[:autos]
    @trailers       = params[:trailers]
    @motorbikes     = params[:motorbikes]
  end
  def search
    require 'uri'
    require 'net/http'

    url = URI("https://ws.trasmediterranea.es/JourneyAvailability.asmx")

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Post.new(url)
    request["content-type"] = 'text/xml'
    request["cache-control"] = 'no-cache'
    request["postman-token"] = '147425b8-c1d5-df44-35ce-664fa48ca6c7'
    request.body = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tras=\"Trasme.WS.ExternalGateway\" xmlns:tras1=\"Trasme.WS.Interfaces\">\n<soapenv:Header>\n<tras:InputHeader>\n<tras:UserName>PortAGF2rr</tras:UserName>\n<tras:Password>3PlAGf2rr3</tras:Password>\n<tras:UserType>4</tras:UserType>\n<tras:ExternalID></tras:ExternalID>\n<tras:ProcessID></tras:ProcessID>\n<tras:Language>1</tras:Language>\n<tras:Version>1</tras:Version>\n</tras:InputHeader>\n</soapenv:Header>\n<soapenv:Body>\n<tras:ListadoDisponibilidad_JourneyAvailability>\n<tras:Request_JourneyAvailability>\n<tras1:Journeys>\n<tras1:Journey>\n<tras1:OutwardDate>\n<tras1:Day>"+@outward_day+"</tras1:Day>\n<tras1:Month>"+@outward_month+"</tras1:Month>\n<tras1:Year>"+@outward_year+"</tras1:Year>\n</tras1:OutwardDate>\n<tras1:OutwardTime>\n<tras1:Hour>"+@outward_hour+"</tras1:Hour>\n<tras1:Minutes>"+@outward_minutes+"</tras1:Minutes>\n</tras1:OutwardTime>\n<tras1:ArriveDate>\n<tras1:Day>"+@arrive_day+"</tras1:Day>\n<tras1:Month>"+@arrive_month+"</tras1:Month>\n<tras1:Year>"+@arrive_year+"</tras1:Year>\n</tras1:ArriveDate>\n<tras1:ArriveTime>\n<tras1:Hour>"+@arrive_hour+"</tras1:Hour>\n<tras1:Minutes>"+@arrive_minutes+"</tras1:Minutes>\n</tras1:ArriveTime>\n<tras1:From>"+@origin+"</tras1:From>\n<tras1:To>"+@destiny+"</tras1:To>\n<tras1:Occupation>\n<tras1:NumPets>"+@pets+"</tras1:NumPets>\n<tras1:Passengers>\n<tras1:Total>"+@passengers+"</tras1:Total>\n<tras1:NumAdults>"+@adults+"</tras1:NumAdults>\n<tras1:NumChildren>"+@children+"</tras1:NumChildren>\n<tras1:NumBabies>"+@babies+"</tras1:NumBabies>\n</tras1:Passengers>\n<tras1:Vehicles>\n<tras1:Total>"+@vehicles+"</tras1:Total>\n<tras1:NumAutos>"+@autos+"</tras1:NumAutos>\n<tras1:NumTrailers>"+@trailers+"</tras1:NumTrailers>\n<tras1:NumMotorbikes>"+@motorbikes+"</tras1:NumMotorbikes>\n</tras1:Vehicles>\n</tras1:Occupation>\n</tras1:Journey>\n</tras1:Journeys>\n</tras:Request_JourneyAvailability>\n</tras:ListadoDisponibilidad_JourneyAvailability>\n</soapenv:Body>\n</soapenv:Envelope>"

    response = http.request(request)
    priceshash = Hash.from_xml(response.body.gsub("\n",""))

    @price_outward_day      = priceshash["Envelope"]["Body"]["ListadoDisponibilidad_JourneyAvailabilityResponse"]["ListadoDisponibilidad_JourneyAvailabilityResult"]["Journeys"]["Journey"][0]["OutwardDate"]["Day"]
    @price_outward_month    = priceshash["Envelope"]["Body"]["ListadoDisponibilidad_JourneyAvailabilityResponse"]["ListadoDisponibilidad_JourneyAvailabilityResult"]["Journeys"]["Journey"][0]["OutwardDate"]["Month"]
    @price_outward_year     = priceshash["Envelope"]["Body"]["ListadoDisponibilidad_JourneyAvailabilityResponse"]["ListadoDisponibilidad_JourneyAvailabilityResult"]["Journeys"]["Journey"][0]["OutwardDate"]["Year"]
    @price_outward_hour     = priceshash["Envelope"]["Body"]["ListadoDisponibilidad_JourneyAvailabilityResponse"]["ListadoDisponibilidad_JourneyAvailabilityResult"]["Journeys"]["Journey"][0]["OutwardTime"]["Hour"]
    @price_outward_minutes  = priceshash["Envelope"]["Body"]["ListadoDisponibilidad_JourneyAvailabilityResponse"]["ListadoDisponibilidad_JourneyAvailabilityResult"]["Journeys"]["Journey"][0]["OutwardTime"]["Minutes"]
    @price_arrive_day       = priceshash["Envelope"]["Body"]["ListadoDisponibilidad_JourneyAvailabilityResponse"]["ListadoDisponibilidad_JourneyAvailabilityResult"]["Journeys"]["Journey"][0]["ArriveDate"]["Day"]
    @price_arrive_month     = priceshash["Envelope"]["Body"]["ListadoDisponibilidad_JourneyAvailabilityResponse"]["ListadoDisponibilidad_JourneyAvailabilityResult"]["Journeys"]["Journey"][0]["ArriveDate"]["Month"]
    @price_arrive_year      = priceshash["Envelope"]["Body"]["ListadoDisponibilidad_JourneyAvailabilityResponse"]["ListadoDisponibilidad_JourneyAvailabilityResult"]["Journeys"]["Journey"][0]["ArriveDate"]["Year"]
    @price_arrive_hour      = priceshash["Envelope"]["Body"]["ListadoDisponibilidad_JourneyAvailabilityResponse"]["ListadoDisponibilidad_JourneyAvailabilityResult"]["Journeys"]["Journey"][0]["ArriveTime"]["Hour"]
    @price_arrive_minutes   = priceshash["Envelope"]["Body"]["ListadoDisponibilidad_JourneyAvailabilityResponse"]["ListadoDisponibilidad_JourneyAvailabilityResult"]["Journeys"]["Journey"][0]["ArriveTime"]["Minutes"]
    @price_origin           = priceshash["Envelope"]["Body"]["ListadoDisponibilidad_JourneyAvailabilityResponse"]["ListadoDisponibilidad_JourneyAvailabilityResult"]["Journeys"]["Journey"][0]["From"]
    @price_destiny          = priceshash["Envelope"]["Body"]["ListadoDisponibilidad_JourneyAvailabilityResponse"]["ListadoDisponibilidad_JourneyAvailabilityResult"]["Journeys"]["Journey"][0]["To"]
    @price_ship_code        = priceshash["Envelope"]["Body"]["ListadoDisponibilidad_JourneyAvailabilityResponse"]["ListadoDisponibilidad_JourneyAvailabilityResult"]["Journeys"]["Journey"][0]["ShipCode"]
    @price_code             = priceshash["Envelope"]["Body"]["ListadoDisponibilidad_JourneyAvailabilityResponse"]["ListadoDisponibilidad_JourneyAvailabilityResult"]["Journeys"]["Journey"][0]["Accommodations"]["Accommodation"][0]["Code"]
    @price_category         = priceshash["Envelope"]["Body"]["ListadoDisponibilidad_JourneyAvailabilityResponse"]["ListadoDisponibilidad_JourneyAvailabilityResult"]["Journeys"]["Journey"][0]["Accommodations"]["Accommodation"][0]["Category"]
    @price_use              = priceshash["Envelope"]["Body"]["ListadoDisponibilidad_JourneyAvailabilityResponse"]["ListadoDisponibilidad_JourneyAvailabilityResult"]["Journeys"]["Journey"][0]["Accommodations"]["Accommodation"][0]["Use"]
    @price_profile          = priceshash["Envelope"]["Body"]["ListadoDisponibilidad_JourneyAvailabilityResponse"]["ListadoDisponibilidad_JourneyAvailabilityResult"]["Journeys"]["Journey"][0]["Accommodations"]["Accommodation"][0]["Profile"]
    @price_pets             = @pets
    @price_total            = @passengers
    @price_adults           = @adults
    @price_children         = @children
    @price_babies           = @babies
    @price_vehicles         = @vehicles
    @price_autos            = @autos
    @price_trailers         = @trailers
    @price_motorbikes       = @motorbikes

    if @price_arrive_day.size == 1
      @price_arrive_day = "0" + @price_arrive_day
    end
    if @price_arrive_month.size == 1
      @price_arrive_month = "0" + @price_arrive_month
    end

    priceurl = URI("https://ws.trasmediterranea.es/Prices.asmx")

    pricehttp = Net::HTTP.new(priceurl.host, priceurl.port)
    pricehttp.use_ssl = true
    pricehttp.verify_mode = OpenSSL::SSL::VERIFY_NONE

    pricerequest = Net::HTTP::Post.new(priceurl)
    pricerequest["content-type"] = 'text/xml'
    pricerequest["cache-control"] = 'no-cache'
    pricerequest["postman-token"] = 'c039a6c8-d57c-3f0e-af06-94e751d9ef4a'
    pricerequest.body =  "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tras=\"Trasme.WS.ExternalGateway\" xmlns:tras1=\"Trasme.WS.Interfaces\">\n<soapenv:Header>\n<tras:InputHeader>\n<tras:UserName>PortAGF2rr</tras:UserName>\n<tras:Password>3PlAGf2rr3</tras:Password>\n<tras:UserType>4</tras:UserType>\n<tras:ExternalID></tras:ExternalID>\n<tras:ProcessID></tras:ProcessID>\n<tras:Language>1</tras:Language>\n<tras:Version>1</tras:Version>\n</tras:InputHeader>\n</soapenv:Header>\n<soapenv:Body>\n<tras:Precios_Prices>\n<tras:Request_Prices>\n<tras1:Journeys>\n<tras1:Journey>\n<tras1:OutwardDate>\n<tras1:Day>"+@price_outward_day+"</tras1:Day>\n<tras1:Month>"+@price_outward_month+"</tras1:Month>\n<tras1:Year>"+@price_outward_year+"</tras1:Year>\n</tras1:OutwardDate>\n<tras1:OutwardTime>\n<tras1:Hour>"+@price_outward_hour+"</tras1:Hour>\n<tras1:Minutes>"+@price_outward_minutes+"</tras1:Minutes>\n</tras1:OutwardTime>\n<tras1:ArriveDate>\n<tras1:Day>"+@price_arrive_day+"</tras1:Day>\n<tras1:Month>01</tras1:Month>\n<tras1:Year>"+@price_arrive_year+"</tras1:Year>\n</tras1:ArriveDate>\n<tras1:ArriveTime>\n<tras1:Hour>"+@price_arrive_hour+"</tras1:Hour>\n<tras1:Minutes>"+@price_arrive_minutes+"</tras1:Minutes>\n</tras1:ArriveTime>\n<tras1:From>"+@price_origin+"</tras1:From>\n<tras1:To>"+@price_destiny+"</tras1:To>\n<tras1:ShipCode>"+@price_ship_code+"</tras1:ShipCode>\n<tras1:Accommodations>\n<!--Zero or more repetitions:-->\n<tras1:Accommodation>\n<tras1:Code>"+@price_code+"</tras1:Code>\n<tras1:Category>"+@price_category+"</tras1:Category>\n<tras1:Use>"+@price_use+"</tras1:Use>\n<tras1:Profile>"+@price_profile+"</tras1:Profile>\n<tras1:TariffCode></tras1:TariffCode>\n<tras1:Advantages>\n</tras1:Advantages>\n<tras1:Occupation>\n<tras1:NumPets>"+@price_pets+"</tras1:NumPets>\n<tras1:Passengers>\n<tras1:Total>"+@price_total+"</tras1:Total>\n<tras1:NumAdults>"+@price_adults+"</tras1:NumAdults>\n<tras1:NumChildren>"+@price_children+"</tras1:NumChildren>\n<tras1:NumBabies>"+@price_babies+"</tras1:NumBabies>\n</tras1:Passengers>\n<tras1:Vehicles>\n<tras1:Total>"+@price_vehicles+"</tras1:Total>\n<tras1:NumAutos>"+@price_autos+"</tras1:NumAutos>\n<tras1:NumTrailers>"+@price_trailers+"</tras1:NumTrailers>\n<tras1:NumMotorbikes>"+@price_motorbikes+"</tras1:NumMotorbikes>\n</tras1:Vehicles>\n</tras1:Occupation>\n</tras1:Accommodation>\n</tras1:Accommodations>\n</tras1:Journey>\n</tras1:Journeys>\n</tras:Request_Prices>\n</tras:Precios_Prices>\n</soapenv:Body>\n</soapenv:Envelope>"

    priceresponse = http.request(pricerequest)
    ferry = priceresponse.read_body
  end
end
